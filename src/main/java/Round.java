package main.java;

import java.util.Hashtable;
import java.util.Scanner;


public class Round extends Thread {
	
	private static Hashtable<String, Bet> bets = new Hashtable<String, Bet>();
	
	public static Hashtable<String, Bet> getBets() {
		return bets;
	}

	public Round(Hashtable<String, Bet> bets) {
		this.bets = bets;
	}

	@Override
	public void run() {
		
		while(true){
			playRound();
		}
	} 

	public static void playRound() { 
		
		Scanner input = new Scanner(System.in); 
		System.out.println("Please pick User name, a number and quantity"); 
		String user = input.next(); 
		String number = input.next();
		Double dAmount = input.nextDouble(); 
		Bet bet = new Bet(number, dAmount);
		getBets().put(user, bet);
	} 
}