package main.java;

public class Bet {
	
	private String number;
	private double amount;
	
	public Bet(String number, double amount) {
		super();
		this.number = number;
		this.amount = amount;
	}
	public Bet() {
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

}
