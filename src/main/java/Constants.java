package main.java;

public class Constants {

	public static final String ODD = "ODD";
	public static final String EVEN = "EVEN";
	public static final int ODD_VALUE = -1;
	public static final int EVEN_VALUE = -2;
	public static final String WIN = "WIN";
	public static final String LOSE = "LOSE";
}
