package main.java;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Hashtable<String, Bet> bets = new Hashtable<String, Bet>();
		// read file
		try (BufferedReader in = new BufferedReader(new FileReader(args[0]))) {
			String user;
			while ((user = in.readLine()) != null) {
				bets.put(user, new Bet());
			}
			Round round = new Round(bets);
			round.start();
			Thread.sleep( 30 * 1000 ); // wait 30 sec
			round.interrupt();
			// process result
			processResult(Round.getBets());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void processResult(Hashtable<String, Bet> bets) {
		Random generator = new Random(); 
		int roulette = 1 + generator.nextInt(35); 
		System.out.println("Number: " + roulette);
		System.out.println("Player		Bet	Outcome	Winnings");
		System.out.println("---");

		Enumeration<String> e = bets.keys();
		String key;
		Bet bet;
		while( e.hasMoreElements() ){
			double money = 0; 
			key = (String) e.nextElement();
			bet = bets.get( key );
			if(roulette == 0){
				System.out.println( key + "		" + bet.getNumber() +  "	"  + Constants.LOSE +  "	"  + money); 
			}
			else if((roulette%2 == 0 && bet.getNumber().equals(Constants.EVEN)) || (roulette%2 == 1 && bet.getNumber().equals(Constants.ODD)) ){ 
				money = money + bet.getAmount()*2;
				System.out.println( key + "		" + bet.getNumber() +  "	"  + Constants.WIN +  "	"  + money); 

			}else if(!bet.getNumber().equals(Constants.EVEN) && !bet.getNumber().equals(Constants.ODD) && Integer.parseInt(bet.getNumber())==roulette){ 
				money = money + bet.getAmount()*36;
				System.out.println( key + "		" + bet.getNumber() +  "	"  + Constants.WIN +  "	"  + money); 

			}else{ 
				System.out.println( key + "		" + bet.getNumber() +  "	"  + Constants.LOSE +  "	"  + money); 
			} 
		}
	}
}
