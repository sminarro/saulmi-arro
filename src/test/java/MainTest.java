/**
 * 
 */
package test.java;

import java.io.File;
import java.util.Hashtable;

import main.java.Bet;
import main.java.Main;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Sa�l
 *
 */
public class MainTest {
	
	private static File tempFile;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void createTestFile() throws Exception {
		tempFile = File.createTempFile("users", ".txt");
		tempFile.deleteOnExit();
	}

	
	@Test
	public void loadMultiPlayerFileTest() throws Exception {
//		String[] args = {"src/main/java/users.txt"};
//		new Main();
//		Main.main(args);

	}
	
	/**
	 * Test method for {@link main.java.Main#processResult(java.util.Hashtable)}.
	 */
	@Test
	public final void testProcessResult() {
		Hashtable<String, Bet> bets = new Hashtable<String, Bet>();
		bets.put("uno", new Bet("1",1));
		bets.put("dos", new Bet("ODD",1));
		bets.put("tres", new Bet("EVEN",1));
		
		Main.processResult(bets);
	}

}
